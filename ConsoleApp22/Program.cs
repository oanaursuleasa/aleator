﻿using System;

namespace ConsoleApp22
{
    class Program
    {
        private static int m,n;

        static void Main(string[] args)
        {

            
            int[,] matrice = new int[2, 5];
            for (int m = 0; m < matrice.GetLength(0); m++)
            {
                for (int n = 0; n < matrice.GetLength(1); n++)
                {
                    matrice[m, n] = (m + 1) * (n + 1);
                }
                Console.WriteLine();
            }
            Console.WriteLine(matrice.Length);

            //afisam matricea

            for(int m=0; m<matrice.GetLength(0); m++)
            {
                for ( int n = 0; n < matrice.GetLength(1); n++)
                {
                    Console.Write(matrice[m, n]);
                    if (n != matrice.GetLength(1) - 1)
                        Console.Write(",");
                }
                Console.WriteLine();
            }

            //suma matricii pe diagonala normala

            for (int n = 0; n < matrice.GetLength(0); n++)
            {
                Console.Write(matrice[n, n]);
            }
            Console.WriteLine();
             

            // suma matricii de pe diagonala inversa

            for(int n=0; n<matrice.GetLength(0); n++)
            {
                Console.Write(matrice[n, matrice.GetLength(0) - n - 1].ToString() + "");
            }
            Console.WriteLine();


            //repetarea nr in vector 

            int[][] nr = new int[matrice.Length][];
          for (int n=0;n<matrice.Length;n++)
            {
                nr[n] = new int[0];
                string input = Console.ReadLine();
                while(input!="next")
                {
                    Array.Resize(ref nr[n], nr[n].Length + 1);
                    nr[n][nr[n].Length - 1] = int.Parse(input);
                    input = Console.ReadLine();
                }
            }

        }
    }
}
